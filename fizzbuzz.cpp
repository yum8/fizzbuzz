#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cmath>
#include <iostream>

bool is_prime(int n){
    if (n == 2) {
        return true;
    }
    if (n < 2 || n % 2 == 0) {
        return false; // not a prime if it is less than 2 or if it is an even number greater than 2
    }

    // Since we've already eliminate numbers that are multiples of two, we only
    // need to see if the given number is divisible by an odd numbers
    for (int i = 3; i <= sqrt(n); i += 2){
        if (n % i == 0 ) {
            return false;
        }
    }

    return true;
}

void fizz_buzz(int n) {
    assert(n >= 0);
    int left = 1, right = 1, num = 1;

    for (int i = 1; i <= n; ++i) {
        if (i > 2) {
            num = left + right;
            left = right;
            right = num;
        } else {
            num = 1;
        }

        if (num % 15 == 0) {
            printf("FizzBuzz\n");
        } else if (num % 3 == 0) {
            printf("Buzz\n");
        } else if (num % 5 == 0) {
            printf("Fizz\n");
        } else if (is_prime(num)) {
            printf("BuzzFizz\n");
        } else {
            printf("%d\n", num);
        }
    }
}

int main(void) {
    int n = 0;
    printf("enter n: ");
    std::cin >> n;
    fizz_buzz(n);
    return EXIT_SUCCESS;
}
